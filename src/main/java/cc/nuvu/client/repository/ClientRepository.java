package cc.nuvu.client.repository;

import cc.nuvu.client.Constant;
import cc.nuvu.client.model.Client;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ClientRepository extends JpaRepository<Client, Long> {

    //@Query("Select C FROM " + Constant.ENTITY_TABLE_CLIENT_NAME +" C WHERE C." + Constant.ENTITY_TABLE_CLIENT_COLUMN_DOCUMENT_NUMBER + " like %?1")
    Optional<Client> findByDocumentNumber(String numberDocument);
}
