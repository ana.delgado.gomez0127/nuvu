package cc.nuvu.client.service;

import cc.nuvu.client.dto.ClientDto;
import cc.nuvu.client.dto.CreditCardDto;
import cc.nuvu.client.model.Client;
import cc.nuvu.client.model.CreditCard;
import cc.nuvu.client.repository.ClientRepository;
import cc.nuvu.client.repository.CreditCardRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;

@Service
public class CreditCardService {

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private ClientRepository clientRepository;

    @Autowired
    private CreditCardRepository creditCardRepository;

    public ClientDto saveClient(ClientDto clientDto){

        var client = modelMapper.map(clientDto, Client.class);
        client = clientRepository.save(client);
        clientDto = modelMapper.map(client, ClientDto.class);
        clientDto.setCreditCardDtos(new HashSet<>());

        for (CreditCard creditCard : client.getCreditCard()) {
            CreditCardDto auxCreditCard = modelMapper.map(creditCard, CreditCardDto.class);
            clientDto.getCreditCardDtos().add(auxCreditCard);
        }

        return clientDto;
    }

    public List<ClientDto> getAllClient(){

        List<ClientDto> list = new ArrayList<>();

        for (Client client : clientRepository.findAll()) {

            ClientDto auxClient = modelMapper.map(client, ClientDto.class);
            auxClient.setCreditCardDtos(new HashSet<>());

            for (CreditCard creditCard : client.getCreditCard()) {
                CreditCardDto auxCreditCard = modelMapper.map(creditCard, CreditCardDto.class);
                auxClient.getCreditCardDtos().add(auxCreditCard);
            }

            list.add(auxClient);
        }
        return list;
    }

    public ClientDto getClientSearchByNumberDocument(String numberDocument){

        Optional<Client> client = clientRepository.findByDocumentNumber(numberDocument);

        if(client.isPresent()){

            ClientDto auxClient = modelMapper.map(client.get(), ClientDto.class);
            auxClient.setCreditCardDtos(new HashSet<>());

            for (CreditCard creditCard : client.get().getCreditCard()) {
                CreditCardDto auxCreditCard = modelMapper.map(creditCard, CreditCardDto.class);
                auxClient.getCreditCardDtos().add(auxCreditCard);
            }

            return auxClient;
        }
        else{
            return null;
        }
    }

    public ClientDto updateDataClientById(ClientDto clientDto, Long numberDocument){

        if(clientRepository.findById(numberDocument).isPresent()){

            var auxUpdateClient = modelMapper.map(clientDto, Client.class);
            auxUpdateClient.setId(numberDocument);
            var client = clientRepository.save(auxUpdateClient);
            ClientDto auxClient = modelMapper.map(client, ClientDto.class);
            auxClient.setCreditCardDtos(new HashSet<>());

            for (CreditCard creditCard : client.getCreditCard()) {
                CreditCardDto auxCreditCard = modelMapper.map(creditCard, CreditCardDto.class);
                auxClient.getCreditCardDtos().add(auxCreditCard);
            }
            return auxClient;
        }
        else{
            return null;
        }
    }

}
