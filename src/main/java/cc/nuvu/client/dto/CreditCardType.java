package cc.nuvu.client.dto;

public enum CreditCardType {
    VISA, MASTER, AMERICAN_EXPRESS;
}
