package cc.nuvu.client.dto;

import lombok.Getter;

@Getter
public enum DocumentType {

    CEDULA_CIUDADANIA, CEDULA_EXTRANJERIA;
}
